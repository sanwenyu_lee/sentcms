<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\controller\api;

use app\model\Category;

/**
 * @title 内容管理
 */
class Content extends Base {

	/**
	 * @title 内容列表
	 * @method GET
	 * @param  Category $category [description]
	 * @return [json]
	 */
	public function lists(Category $category){

	}

	/**
	 * @title 内容详情
	 * @method GET
	 * @return [json]
	 */
	public function detail(){

	}

	/**
	 * @title 添加内容
	 * @method POST
	 * @return [json]
	 */
	public function add(){

	}

	/**
	 * @title 修改内容
	 * @method POST
	 * @return [json]
	 */
	public function edit(){

	}

	/**
	 * @title 删除内容
	 * @method POST
	 * @return [json]
	 */
	public function delete(){

	}
}